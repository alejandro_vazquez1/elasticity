#!/usr/bin/env python3

"""
Implementation of the pressure on a given msh
link to fenics exampel:
    https://fenicsproject.org/pub/tutorial/html/._ftut1008.html
"""

import numpy as np
from fenics import *
from ufl import nabla_div

def pressure():
    """
    returns solution to write into .pvd 
    with pressure on given mesh
    """
    # Default mesh
    ## TO DO:   - use mesh given as argument to function
    ##          - only works in x-direction
    ##          - force looks like it is only applied to one point and not 
    ##             uniform over whole surface
    ##          - force from lower end

    L = 1; W = 0.2
    mesh = BoxMesh(Point(0, 0, 0), Point(L, W, W), 10, 3, 3) 
    
    # Scaled variables
    
    mu = 1
    rho = 20
    delta = W/L
    gamma = 0.4*delta**2
    
    beta = 1.25
    lambda_ = beta
    g = gamma


    # Define function Space
    V = VectorFunctionSpace(mesh, 'CG', 1)
    
    # Define boundary condition
    tol = 1e-14


    def foundation(x, on_boundary):
        return on_boundary and x[0] < tol
    
    bc = DirichletBC(V, Constant((0, 0, 0)), foundation)
    #        AutoSubDomain(lambda x: near(x[0], 0)))

    # Define strain and stress
    def epsilon(u):
        return sym(nabla_grad(u))

    def sigma(u):
        return lambda_*nabla_div(u)*Identity(d) + 2*mu*epsilon(u)
    
    # Define variational problem
    u = TrialFunction(V)
    d = u.geometric_dimension() # space dimension
    v = TestFunction(V)
    f = Constant((-rho*g,0,0)) # force in z-direction
    T = Constant((0,0,0))
    a = inner(sigma(u), epsilon(v))*dx
    L = dot(f,v)*dx + dot(T,v)*ds

    # Compute solution
    u = Function(V)
    solve(a == L, u, bc)

    #return solution
    return u # this is the displacement vector field

if __name__ == "__main__":
    # save solution
    u = pressure()
    f = File("pressure.pvd")
    f << u;
    print("Solution written in './pressure.pvd' with defaul mesh")

