import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

def w(xArr):
    return F/(E*I)*(L*xArr**2/2. - xArr**3/6.)
 

# Constants all in si
F = -825.  
E = 65e9 
I = 2.13e-7
L = 1.  #length beam


x = np.linspace(0,1,100)
y = w(x)


# Maximale Auslenkung des Balkens:
f = w(L)
print("Maximale Auslenkung: {0:0.2e} m".format(f))

plt.plot(x,y, label="Biegelinie w(x)")
plt.plot(x,np.zeros(100), label="ohne Gewicht")

plt.ylabel("Auslenkung (m)")
plt.xlabel("Balkenlänge (m)")

plt.title("Auslenkung des Kragbalken in m")
plt.legend()
plt.ylim((-0.2,0.2))

plt.tight_layout()
#plt.grid()
plt.savefig("./img/biegelinie.png")
plt.close()

"""vergleich fenics zu rechnung:"""

df = pd.read_csv("fenics/displacement.csv")
col = df.columns
# neutrale Faser der Fenics Lösung


print(df[col[1]])
## plot comparission
plt.plot(x*1000,y*1000, label="Biegelinie, wie errechnet")
plt.plot(df[col[1]]*-1,"--",label="neutrale Faser der Fenics-Lösung")
plt.xlabel("Balkenlänge (mm)")
plt.ylabel("Auslenkung (mm)")
plt.legend()
#plt.show()
plt.savefig("./img/comparison.png")


