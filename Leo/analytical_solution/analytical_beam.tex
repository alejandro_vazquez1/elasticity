\documentclass[a4paper,DIV=13,12pt]{scrartcl}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}         
\usepackage{ngerman}             
\usepackage{float}
\usepackage[pdftex]{graphics} 
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[bf]{caption}
\usepackage{siunitx} 
\sisetup{separate-uncertainty=true,
exponent-product = \cdot,} 
\usepackage{scrlayer-scrpage}
\usepackage{color}
\usepackage{fancybox}	
\usepackage[colorlinks,	% Farben der Links -> diese werden dann mit \url{} gekennzeichnet (sh. Literaturverzeichnis)
						linkcolor=blue,
						urlcolor=blue]{hyperref}

%\definecolor{hellgrau}{gray}{0.85}
\setlength{\parindent}{0cm}
\setlength{\parskip}{0.5cm}                       

\cfoot{\pagemark}



\title{Analytische Lösung des Kragbalken}
\subtitle{im Vergleich mit der Biegelinie einer Fenics-Lösung}
\author{Gruppe 03}

\begin{document}

% Titelseite
\maketitle
\newpage

\section{Theoretische Lösung}
\subsection{Der Euler-Bernoulli-Balken}
Ein Euler-Bernoulli-Balken ist eine Vereinfachung aus der technischen Mechanik. Folgende Annahmen werden hierbei an einem deformierbaren Balken festgesetzt:\cite{wikipedia}
\begin{itemize}
	\item Der Balken ist schlank, seine Länge ist wesentlich Größer als sein Querschnitt
	\item Balkenquerschnitte die vor der Verformung senkrecht auf die Balkenachse stehen, stehen nach der Deformation senkrecht auf die verformte Achse
	\item Die Querschnitte bleiben auch nach der Deformation in sich eben
\end{itemize}
Diese Annahmen werden für alle folgenden Rechnungen verwendet.

\subsection{Die Biegelinie und ihre Differentalgleichung}
Die neutrale Faser bezeichnet in der Festigkeitslehre diejenige Faser oder Schicht eines Balkenquerschnitts, deren Länge sich beim Verdrehen bzw. Biegen nicht ändert. Dort verursacht die Beanspruchung keine Zug- oder Druck-Spannung.

Die Biegeline $w(x)$ des Ortpunktes $x$ gibt Aufschluss darüber, wie sich die neutrale Faser eines belasteten Balken darstellt, wobei angenommen wird, dass der Balken konstante Momente hat und seine Biegesteifigkeiten sich nicht abrupt verändert. 

Im Folgendem wird die Herleitung der Differentialgleichung der Biegelinie skizziert. Für eine genauere Ausführung wird auf \cite{TM-Buch} verwiesen. 
Durch den Zusammenhang der abgeleiteten Biegelinie $w'(x) = \tan(\varphi)$ mit dem Biegewinkel lassen sich die folgenden äquivalenten Beziehung herstellen:

\begin{equation}
\varphi = \arctan(w') \to \cos(\varphi) = \frac{1}{\sqrt{1+w'^2}}
\end{equation}

Durch die Betrachtung eines ausreichend kleinen Längenelements $\Delta s$ des gekrümmten Balkens ergibt sich bei annähernd konstanten Biegemoment $M(x)$, Elastizitätsmodul $E(x)$ und Flächenträgheitsmoment $I(x)$ der Zusammenhang

\begin{equation}
\frac{1}{\rho (x)} = \frac{M(x)}{EI(x)}.
\end{equation}

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{./img/balkenelement.png}
\caption{Skizze des verformten Balkenelements}
\label{fig:balkenelement}
\end{figure}

Aus Abbildung \ref{fig:balkenelement} ersichtliche Abhängigkeiten führen daraufhin zu
\begin{equation}
\varphi'(x) = -\frac{1}{\rho\cos(\varphi)}.
\end{equation}

Das Einsetzen der obigen Ausdrücke für $\varphi$, $\cos(\varphi)$ und $\frac{1}{\rho}$ führt zur nichtliniearen Differentialgleichung zweiter Ordnung

\begin{equation}
\frac{w''}{(1+w'^2)^{(3/2)}} = - \frac{M}{EI}.
\end{equation}

Durch Annahme eines kleinen Biegewinkels $|\varphi| << 1$, lässt sich $1 + w'^2 \approx 1$ setzen, was schlussendlich zur linearen Differentalgleichung der Biegelinie $w(x)$ führt:

\begin{equation}
w''(x) = -\frac{M}{EI}
\label{eq:biegelinie}
\end{equation}

\subsection{Problemmstellung und Lösung}
Im Folgendem wird nun die eindimensionale Biegelinie eines Kragbalken aus Aluminium mit den Abmessungen $(\num{1,00}\cdot \num{0,04} \cdot \num{0,04})\si{m}$ und dem entsprechenden Elastizitätsmodul von $E = \SI{65}{\giga\pascal}$ \cite{emodul} betrachtet. Die Grundfläche des Balken wird dabei als quadratisch angenommen, was zu einem Flächenträgheitsmoment von $I_z = \frac{b^4}{12} = \SI{2,1}{10^{-7}m^4}$ führt.  Am freien Ende des Balkens wird ein Gewicht montiert, das im Erdschwerefeld mit einer Kraft von etwa $F = \SI{-825}{N}$ am Balkenende zieht. Der Balken selbst wird als gewichtslos angenommen.

%Anmerkung: Tonnen Gewicht angehängt, um bei den neuen Balkendimensionen einen Verbiegung im Mikrometerberech feststellen zu können

Der Momentverlauf $M(x)$ über den Balken wird durch durch die Gleichung \eqref{eq:momentverlauf} gegeben. $l$ repräsentiert die Länge des Balkens von $\SI{1}{m}$.
\begin{equation}
M(x) = -(l-x)\cdot F
\label{eq:momentverlauf}
\end{equation}

Wird \eqref{eq:momentverlauf} in \eqref{eq:biegelinie} eingesetzt, kann die Differentialgleichung des gegebenen Problems gelöst werden. Hierbei wird für die Anfangsbedingungen (Aufhängungshöhe des Balkens $w_0$ und Biegewinkel $\varphi_0$) jeweils $0$ angenommen:

\begin{equation}
w(x) = \frac{F}{EI}\int_0^x (l\xi - \xi^2/2)d\xi = \frac{F}{EI}\left(\frac{lx^2}{2}-\frac{x^3}{6}\right)
\label{eq:loesung}
\end{equation}

Die eingesetzten Zahlenwerte ergeben somit die Biegelinie unter Belastung, welche durch die Abbildung \ref{fig:biegung} zusammen mit der des unbelasteten Balken unterhalb dargestellt wird.

\begin{figure}[H]
\centering
\includegraphics[scale=0.8]{./img/biegelinie.png}
\caption{Neutrale Faser des unbelasteten und belasteten Alubalkens}
\label{fig:biegung}
\end{figure}

Die maximale Balkenauslenkung $f$ lässt sich durch das einsetzten der Balkenlänge in die Gleichung der Biegelänge \eqref{eq:loesung} zu
\begin{equation}
f = \frac{Fl^3}{3EI} =\SI{-1,99}{cm}
\end{equation}
bestimmen.

\section{Fenics}
\subsection{Implementierung}
Um Vergleichbarkeit über das Projekt zu gewähren wurde das oben beschriebene Problem mit den gleichen Mesh und Programmcode behandelt, wie unsere anderen Ausarbeitungen. Die gesetzten Parameter für Aluminium sind $mu = \num{27}$, $rho = \SI{2.7}{10^{-6}kg\per\mm^3}$, $\_lambda = \num{51.53917}$.

Das verwendete Meshfile wurde mit Gmsh erstellt und als xml mit einer Knotendichte von $cl1 = 5.0$ eingelesen. Seine Abmessungen betragen $(1000\cdot 40\cdot40)\si{mm}$. Die Verschiebekraft wurde aufgrund der angenommenen Masselosigkeit des Balkens als Scherkraft behandelt, welche nur an der rechten, freihängenden Subdomain-Fläche gleichmäßig verteilt wirkt und über das $T$ im Code angewandt wird. Die rechte, festmontierte Seite wird über eine Subdomain-Fläche mit einer Dirichletrandbedingung von $0$ gesetzt. Abbildung \ref{fig:modell} zeigt die Darstellung des gelösten Problems in Paraview, wobei Farben  die Größe der Verschiebung kennzeichnen.


\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{./img/Modell.png}
\caption{Darstellung des Balkens in Paraview}
\label{fig:modell}
\end{figure}

\subsection{Direkter Vergleich mit analytischer Rechnung}
Liest man die Verschiebung der Mittellilie aus Abbildung \ref{fig:modell} aus und trägt sie gegen die berechnete Biegelinie auf, wie in Abbildung \ref{fig:vergleich} dargesetellt wird, so erkennt man eine nahezu perfekte Übereinstimmung mit der theoretischen Verbiegung. Das ist insbesondere auf die hohe Knotendichte und die vergleichsweise geringe Auflösung des Graphen im Millimeterbereich zurückzuführen. Aufgrund der Abmessung und Knotendichte des verwendeten Mesh ergeben sich längs der Biegelinie etwa $\num{200}$ - Knotenpunkte. Zum Vergleich: Graph \ref{fig:biegung} wurde an $\num{100}$ Punkten geplottet.

Daraus lässt sich schließen, dass unsere Modelle nur einen sehr geringen Fehler bezüglich der Diskretisierung aufweisen.

\begin{figure}[H]
\centering
\includegraphics[scale=0.8]{./img/comparison.png}
\caption{Direkter Vergleich der errechneten und ausgelesenen Biegelinie des gegebenen Problems}
\label{fig:vergleich}
\end{figure}

\newpage
\begin{thebibliography}{9}
\bibitem{wikipedia}
\url{https://de.wikipedia.org/wiki/Bernoullische_Annahmen} (Zugriff: 6.1.2021)

\bibitem{emodul}
\url{https://www.electrical-contacts-wiki.com/index.php/Physikalische_Eigenschaften_der_wichtigsten_Metalle} (Zugriff: 7.1.2021)

\bibitem{fenics}
\url{https://fenicsproject.org/pub/tutorial/html/._ftut1008.html} \\(Zugriff: 14.1.2021)

\bibitem{TM-Buch}
Brommundt E., Sachs G. und Sachau D.
\textit{Technische Mechanik: Statik-Elastostatik-Kinematik-Kinetik}.  
Berlin/München/Boston: Walter de Gruyter GmbH, 2019.

\end{thebibliography}

\end{document}
