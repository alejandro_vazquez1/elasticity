import fenics as fe
import numpy as np
import matplotlib.pyplot as plt


class ElastodynamicsProblem:
    def __init__(self, mesh, bc, p, dx=fe.dx, ds=fe.ds, T=4.0, Nsteps=50, E=1000.0, nu=0.3, 
                 rho=1.0, eta_m=0.0, eta_k=0.0, alpha_m=0.2, alpha_f=0.4):
        self.mesh = mesh
        
        # Elastic parameters
        self.E  = E
        self.nu = nu
        self.mu    = fe.Constant(E / (2.0*(1.0 + nu)))
        self.lmbda = fe.Constant(E*nu / ((1.0 + nu)*(1.0 - 2.0*nu)))
        
        # Mass density
        self.rho=fe.Constant(rho)
        
        # Rayleigh damping coefficients
        self.eta_m = fe.Constant(eta_m)
        self.eta_k = fe.Constant(eta_k)
        
        # Generalized-alpha method parameters
        self.alpha_m = fe.Constant(alpha_m)
        self.alpha_f = fe.Constant(alpha_f)
        self.gamma   = fe.Constant(0.5+alpha_f-alpha_m)
        self.beta    = fe.Constant((self.gamma+0.5)**2/4.)
        
        # Time-stepping parameters
        self.T = T
        self.Nsteps = Nsteps
        self.dt = fe.Constant(T/Nsteps)
        
        self.p = p
        
        # Define function space for displacement, velocity and acceleration
        self.V = fe.VectorFunctionSpace(mesh, "CG", 1)
        # Define function space for stresses
        self.Vsig = fe.TensorFunctionSpace(mesh, "DG", 0)
        
        # Test and trial functions
        self.du = fe.TrialFunction(self.V)
        self.u_ = fe.TestFunction(self.V)
        # Current (unknown) displacement
        self.u = fe.Function(self.V, name="Displacement")
        # Fields from previous time step (displacement, velocity, acceleration)
        self.u_old = fe.Function(self.V)
        self.v_old = fe.Function(self.V)
        self.a_old = fe.Function(self.V)
        
        self.ds = ds
        self.dx = dx
        self.bc = bc(self.V)
        
    def setBC(self, bc):
        self.bc = bc(self.V)
        
    def setP(self, p):
        self.p = p
        
    # Stress tensor
    def __sigma(self, r):
        return 2.0*self.mu*fe.sym(fe.grad(r)) + \
    self.lmbda*fe.tr(fe.sym(fe.grad(r)))*fe.Identity(len(r))
    
    # Mass form
    def __m(self, u, u_):
        return self.rho*fe.inner(u, u_)*fe.dx
    
    # Elastic stiffness form
    def __k(self, u, u_):
        return fe.inner(self.__sigma(u), fe.sym(fe.grad(u_)))*fe.dx
    
    # Rayleigh damping form
    def __c(self, u, u_):
        return self.eta_m*self.__m(u, u_) + self.eta_k*self.__k(u, u_)
    
    # Work of external forces
    def __Wext(self, u_):
        return fe.dot(u_, self.p)*self.ds
    
    
    # Update formula for acceleration
    # a = 1/(2*beta)*((u - u0 - v0*dt)/(0.5*dt*dt) - (1-2*beta)*a0)
    def __update_a(self, u, u_old, v_old, a_old, ufl=True):
        if ufl:
            dt_ = self.dt
            beta_ = self.beta
        else:
            dt_ = float(self.dt)
            beta_ = float(self.beta)
        return (u-u_old-dt_*v_old)/beta_/dt_**2 - (1-2*beta_)/2/beta_*a_old
    
    # Update formula for velocity
    # v = dt * ((1-gamma)*a0 + gamma*a) + v0
    def __update_v(self, a, u_old, v_old, a_old, ufl=True):
        if ufl:
            dt_ = self.dt
            gamma_ = self.gamma
        else:
            dt_ = float(self.dt)
            gamma_ = float(self.gamma)
        return v_old + dt_*((1-gamma_)*a_old + gamma_*a)
    
    def __update_fields(self):
        """Update fields at the end of each time step."""
    
        # Get vectors (references)
        u_vec, u0_vec  = self.u.vector(), self.u_old.vector()
        v0_vec, a0_vec = self.v_old.vector(), self.a_old.vector()
    
        # use update functions using vector arguments
        a_vec = self.__update_a(u_vec, u0_vec, v0_vec, a0_vec, ufl=False)
        v_vec = self.__update_v(a_vec, u0_vec, v0_vec, a0_vec, ufl=False)
    
        # Update (u_old <- u)
        self.v_old.vector()[:], self.a_old.vector()[:] = v_vec, a_vec
        self.u_old.vector()[:] = self.u.vector()
        
    def __avg(self, x_old, x_new, alpha):
        return alpha*x_old + (1-alpha)*x_new
    
    def __local_project(self, v, V, u=None):
        """Element-wise projection using LocalSolver"""
        dv = fe.TrialFunction(V)
        v_ = fe.TestFunction(V)
        a_proj = fe.inner(dv, v_)*self.dx
        b_proj = fe.inner(v, v_)*self.dx
        solver = fe.LocalSolver(a_proj, b_proj)
        solver.factorize()
        if u is None:
            u = fe.Function(V)
            solver.solve_local_rhs(u)
            return u
        else:
            solver.solve_local_rhs(u)
            return
    
    
    def solve(self, fileName, path='.'):
        # Residual
        a_new = self.__update_a(self.du, self.u_old, self.v_old, self.a_old, ufl=True)
        v_new = self.__update_v(a_new, self.u_old, self.v_old, self.a_old, ufl=True)
        self.__avg(self.a_old, a_new, self.alpha_m)
        res = self.__m(self.__avg(self.a_old, a_new, self.alpha_m), self.u_) \
               + self.__c(self.__avg(self.v_old, v_new, self.alpha_f), self.u_) \
               + self.__k(self.__avg(self.u_old, self.du, self.alpha_f), self.u_) \
               - self.__Wext(self.u_)
        a_form = fe.lhs(res)
        L_form = fe.rhs(res)
        
        # Define solver for reusing factorization
        K, res = fe.assemble_system(a_form, L_form, self.bc)
        solver = fe.LUSolver(K, "mumps")
        solver.parameters["symmetric"] = True
        
        # Time-stepping
        time = np.linspace(0, self.T, self.Nsteps+1)
        self.u_tip = np.zeros((self.Nsteps+1,))
        self.energies = np.zeros((self.Nsteps+1, 4))
        E_damp = 0
        E_ext = 0
        self.sig = fe.Function(self.Vsig, name="sigma")
        xdmf_file = fe.XDMFFile(path + '/' + fileName + '.xdmf')
        xdmf_file.parameters["flush_output"] = True
        xdmf_file.parameters["functions_share_mesh"] = True
        xdmf_file.parameters["rewrite_function_mesh"] = False
        
        for (i, dt) in enumerate(np.diff(time)):

            t = time[i+1]
            print("Time: ", t)
        
            # Forces are evaluated at t_{n+1-alpha_f}=t_{n+1}-alpha_f*dt
            self.p.t = t-float(self.alpha_f*self.dt)
        
            # Solve for new displacement
            res = fe.assemble(L_form)
            self.bc.apply(res)
            solver.solve(K, self.u.vector(), res)
        
        
            # Update old fields with new quantities
            self.__update_fields()
        
            # Save solution to XDMF format
            xdmf_file.write(self.u, t)
        
            # Compute stresses and save to file
            self.__local_project(self.__sigma(self.u), self.Vsig, self.sig)
            xdmf_file.write(self.sig, t)
        
            self.p.t = t
            # Record tip displacement and compute energies
            # Note: Only works in serial
            if fe.MPI.comm_world.size == 1:
                self.u_tip[i+1] = self.u(1., 0.05, 0.)[1]
            E_elas = fe.assemble(0.5*self.__k(self.u_old, self.u_old))
            E_kin = fe.assemble(0.5*self.__m(self.v_old, self.v_old))
            E_damp += self.dt*fe.assemble(self.__c(self.v_old, self.v_old))
            # E_ext += assemble(Wext(u-u_old))
            E_tot = E_elas+E_kin+E_damp #-E_ext
            self.energies[i+1, :] = np.array([E_elas, E_kin, E_damp, E_tot])
    
        
    def plotTip(self):
        if fe.MPI.comm_world.size == 1:
            # Plot tip displacement evolution
            time = np.linspace(0, self.T, self.Nsteps+1)
            plt.figure()
            plt.plot(time, self.u_tip)
            plt.xlabel("Time")
            plt.ylabel("Tip displacement")
            plt.ylim(-0.5, 0.5)
            plt.show()
    
    def plotEnergies(self):
        if (fe.MPI.comm_world.rank == 0):
            # Plot energies evolution
            time = np.linspace(0, self.T, self.Nsteps+1)
            plt.figure()
            plt.plot(time, self.energies)
            plt.legend(("elastic", "kinetic", "damping", "total"))
            plt.xlabel("Time")
            plt.ylabel("Energies")
            plt.ylim(0, 0.0011)
            plt.show()
        