"""
***************************************************************************
https://www.electrical-contacts-wiki.com/index.php/Physikalische_Eigenschaften_der_wichtigsten_Metalle
***************************************************************************

Some Materials:
    steel:
        nu = 0.3
        (V2A Steel) E = 180
        rho = 7.8 g/cm^3

    aluminium:
        nu = 0.35
        E = 70
        rho = 2,7 g/cm^3

    glass:
        nu = 0.2
        E = 40
        rho = 2,5 g/cm^3
"""
def _lambda(nu, E):         #first lame constant often named lambda 
    x = nu/(1-2*nu) * 1/(1 + nu) * E 
    return x

def mu(nu, E):              #second lame constant also called Schubmodul(G) here named mu 
    x = E/(2*(1 + nu))
    return x

def material_parameters1(nu, E, rho):
    x = _lambda(nu, E)
    y = mu(nu, E)
    z = rho
    return x, y, z

def material_parameters2(rho, E, G, nu):
    x = _lambda(nu, E)
    y = G
    z = rho
    return x, y, z
