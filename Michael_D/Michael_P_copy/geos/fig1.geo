//Figure 1 Elasticity


cl1 = 5.0;

Point(1) = {-20, 20, 0, cl1};
Point(2) = {20, 20, 0, cl1};
Point(3) = {20, 18, 0, cl1};
Point(4) = {4, 18, 0, cl1};
Point(5) = {4, -18, 0, cl1};
Point(6) = {20, -18, 0, cl1};
Point(7) = {20, -20, 0, cl1};
Point(8) = {-20, -20, 0, cl1};
Point(9) = {-20, -18, 0, cl1};
Point(10) = {-4, -18, 0, cl1};
Point(11) = {-4, 18,0, cl1};
Point(12) = {-20, 18, 0, cl1};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};
Line(6) = {6,7};
Line(7) = {7,8};
Line(8) = {8,9};
Line(9) = {9,10};
Line(10) = {10,11};
Line(11) = {11,12};
Line(12) = {12,1};


Line Loop(1) = {1,2,3,4,5,6,7,8,9,10,11,12};
Plane Surface(1) = {1};

object[] = Extrude{0, 0, 1000}{Surface{1};};
Delete{ Volume{object[1]};}
Surface Loop(1) = {1, object[0],object[2] ,object[3] ,object[4], object[5], object[6], object[7],object[8],object[9],object[10],object[11],object[12],object[13]};

Volume(100) = {1};

Physical Volume(1) = {100};

Physical Surface(1) = {1};
Physical Surface(2) = {object[0]};
