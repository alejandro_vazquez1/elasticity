/square "Balken"

cl1 = 5.0;

Point(1) = {-20, -20, 0, cl1};
Point(2) = {20, -20, 0, cl1};   
Point(3) = {-20, 20, 0, cl1};
Point(4) = {20, 20, 0, cl1}; 

Line(1) = {1,2};
Line(2) = {2,4};
Line(3) = {4,3};
Line(4) = {3,1}; 

Line Loop(1) = {1, 2, 3, 4};
Plane Surface(1) = {1};

square1[] = Extrude{0, 0, 1000}{Surface{1};};

Delete{ Volume{square1[1]};}

Surface Loop(1) = {1, square1[0],square1[2] ,square1[3] ,square1[4], square1[5]};

Volume(100) = {1};
  
Physical Volume(1) = {100};

Physical Surface(1) = {1};
Physical Surface(2) = {square1[0]};
