import fenics as fe
import numpy as np
from Classes.elasticityProblem import ElasticityProblem
from Classes.elastodynamicsProblem import ElastodynamicsProblem
from lame import material_parameters2

geo ='C_bar'       #name of the geometry=name of the xml file eg. square,C_bar, H_bar, T_bar, O_square_bar
mat = 'aluminium'   #plugin material name aswell as parameters of said material which will be used and as 
_rho = 2.7          #which the file will be safed as
_E = 65         
_mu = 27
_nu = 0.34

def clamped_boundary(x, on_boundary):
    tol = 1E-14
    return on_boundary and x[2] < tol
    
def elasticityPr():
    # '''
    # Geometry and constants
    # '''
    geometry = geo
    mesh_file = f"geos/{geometry}.xml"
    L = 1000; W = 40
    mesh = fe.Mesh(mesh_file)
    V = fe.VectorFunctionSpace(mesh, 'P', 1)
    material = mat
    rho = _rho
    E = _E
    mu = _mu
    nu = _nu
    x, y, z = material_parameters2(rho, E, mu, nu)
    _lambda = x 
    
    # '''
    # Normal beam on which the gravitational force is acting
    # '''
    
    bc = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), clamped_boundary)
    f = fe.Constant((0, 0, 0)) 
    T = fe.Constant((0, 0, 0))
    beam = ElasticityProblem(mesh, bc, f, T)
    #beam.solve(f'beamWithGravitationDis_{geometry}', 'Output')
    
    # '''
    # Normal beam on which a torsion is acting
    # '''
   
    # Mark boundary subdomians
    left =  fe.CompiledSubDomain("near(x[2], side) && on_boundary", side = 0.0)
    right = fe.CompiledSubDomain("near(x[2], side) && on_boundary", side = 1000.0)
    # Define Dirichlet boundary (x = 0 or x = 1)
    r = fe.Expression(("scale*((x0 + (x[0] - x0)*cos(theta) - (x[1] - y0)*sin(theta)) - x[0])",
                "scale*((y0 + (x[0] - x0)*sin(theta) + (x[1] - y0)*cos(theta)) - x[1])",
                "scale*0.0"),
                scale = 1, x0 = 0, y0 = 0, theta = np.pi/4, degree=2)
    l = fe.Expression(("scale*((x0 + (x[0] - x0)*cos(theta) - (x[1] - y0)*sin(theta)) - x[0])",
                "scale*((y0 + (x[0] - x0)*sin(theta) + (x[1] - y0)*cos(theta)) - x[1])",
                "scale*0.0"),
                scale = 1, x0 = 0, y0 = 0, theta = -np.pi/4, degree=2)    
    bcl = lambda V : fe.DirichletBC(V, l, left)
    bcr = lambda V : fe.DirichletBC(V, r, right)
    bcs = lambda V : [bcl(V), bcr(V)]
    f = fe.Constant((0, 0, 0))
    beam.setBC(bcs)
    beam.setF(f)
    beam.solve(f'{material}_Torsion_{geometry}', 'Output')
    beam.saveVon_Mises(f'{material}_Torsion_{geometry}VonMises', 'Output')
    
    # '''
    # Normal beam with different forces on the side on the side
    # '''
    # Mark boundary subdomians
    left =  fe.CompiledSubDomain("near(x[2], side) && on_boundary", side = 0.0)
    right = fe.CompiledSubDomain("near(x[2], side) && on_boundary", side = 1000.0)
    
    # pressure
    bcl = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 10)), left)
    bcr = lambda V : fe.DirichletBC(V, fe.Constant((0, 0,-10)), right)
    bcs = lambda V : [bcl(V), bcr(V)]
    beam.setBC(bcs)
    beam.solve(f'{material}_Pressure_{geometry}', 'Output')
    beam.saveVon_Mises(f'{material}_Pressure_{geometry}VonMises', 'Output')
    
    # shear force
    bcl = lambda V : fe.DirichletBC(V, fe.Constant((-10, 0, 0)), left)
    bcr = lambda V : fe.DirichletBC(V, fe.Constant((10, 0, 0)), right)
    bcs = lambda V : [bcl(V), bcr(V)]
    beam.setBC(bcs)
    beam.solve(f'{material}_ShearForce_{geometry}', 'Output')
    beam.saveVon_Mises(f'{material}_ShearForce_{geometry}VonMises', 'Output')
    
    # # bending
    # bcl = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0.1)), left)
    # # def middle(x): return fe.near(x[0], L/2) and (fe.near(x[1], 0.) or fe.near(x[1], W)) and (fe.near(x[2], 0.) or fe.near(x[2], W))
    # def middle(x): return fe.near(x[2], 0.) 
    # bcm = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), middle)
    # bcr = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0.1)), right)
    # bcs = lambda V : [bcl(V), bcm(V), bcr(V)]
    # beam.setBC(bcs)
    # beam.solve('beamWithBending', 'Output')

def elastodynamicsPr():
    # Define mesh
    #mesh = fe.BoxMesh(fe.Point(0., 0., 0.), fe.Point(0.04, 0.1, 1.), 60, 10, 5)

    mesh = Mesh(mesh_file)
    # Time-stepping parameters
    T       = 4.0
    Nsteps  = 50
    dt = fe.Constant(T/Nsteps)
    
    p0 = 1.
    cutoff_Tc = T/5
    # Define the loading as an expression depending on t
    p = fe.Expression(("0", "t <= tc ? p0*t/tc : 0", "0"), t=0, tc=cutoff_Tc, p0=p0, degree=0)

    # Sub domain for clamp at left end
    def left(x, on_boundary):
        return fe.near(x[2], 0.) and on_boundary
    
    # Sub domain for rotation at right end
    def right(x, on_boundary):
        return fe.near(x[2], 1.) and on_boundary
    
    # Create mesh function over the cell facets
    boundary_subdomains = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    boundary_subdomains.set_all(0)
    force_boundary = fe.AutoSubDomain(right)
    force_boundary.mark(boundary_subdomains, 3)
    
    # Define measure for boundary condition integral
    dss = fe.ds(subdomain_data=boundary_subdomains)
    
    # Set up boundary condition at left end
    zero = fe.Constant((0.0, 0.0, 0.0))
    bc = lambda V : fe.DirichletBC(V, zero, left)
    beamDynamics = ElastodynamicsProblem(mesh, bc, p, ds=dss(3))
    beamDynamics.solve('elastodynamicsTest1_square', 'Output')
    
    beamDynamics.plotEnergies()
    beamDynamics.plotTip()
    
    
    #change force

def elasticityProblemForces():
    # '''
    # Geometry and constants
    # '''
    # The force is the gravitational force m*g divided by the volume
    # --> rho*g
    L = 1000; W = 40
    geometry = geo
    mesh_file = f"geos/{geometry}.xml"
    mesh = fe.Mesh(mesh_file)
    delta = W/L
    g = 0.4*delta**2
    material = mat
    rho = _rho
    
     # '''
     # Normal beam with different forces on the side 
     # '''

    # Sub domain left end
    def left(x, on_boundary):
        return fe.near(x[2], 0.) and on_boundary
    
    # Sub domain right end
    def right(x, on_boundary):
        return fe.near(x[2], 1000.) and on_boundary
    
    # Sub domain middle end
    def middle(x, on_boundary):
        return on_boundary and fe.between(x[2], (490., 510.))
    
    # Normal beam on which the gravitational force is acting    
    bc = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), left)
    f = fe.Constant((-g*rho*0.001, 0, 0))
    T = fe.Constant((0, 0, 0))
    beam = ElasticityProblem(mesh, bc, f, T)
    beam.solve(f'{material}_gravitationalforce_{geometry}', 'Output')   
    beam.saveVon_Mises(f'{material}_gravitationalforce_{geometry}VonMises', 'Output')   
    beam.saveMagnitude(f'{material}_gravitationalforce_{geometry}Magnitude', 'Output')  

     
if __name__ == "__main__":
    elasticityPr()
    elasticityProblemForces()
    #elastodynamicsPr() 

    
    

   
