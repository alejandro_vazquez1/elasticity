"""
FEniCS tutorial demo program: Linear elastic problem.
  -div(sigma(u)) = f
The model is used to simulate an elastic beam clamped at
its left end and deformed under its own weight.
"""

from __future__ import print_function
import fenics as fe
from ufl import nabla_div
import matplotlib.pyplot as plt
import matplotlib.tri as tri
import numpy as np


# Scaled variables
L = 1; W = 0.2
mu = 1
rho = 1
delta = W/L
gamma = 0.4*delta**2
beta = 1.25
lambda_ = beta
g = gamma

# Create mesh and define function space
mesh = fe.BoxMesh(fe.Point(0, 0, 0), fe.Point(L, W, W), 10, 3, 3)
V = fe.VectorFunctionSpace(mesh, 'P', 1)

# Define boundary condition
tol = 1E-14

def clamped_boundary(x, on_boundary):
    return on_boundary and x[0] < tol



bc = fe.DirichletBC(V, fe.Constant((0, 0, 0)), clamped_boundary)

# Define strain and stress

def epsilon(u):
    return 0.5*(fe.nabla_grad(u) + fe.nabla_grad(u).T)
    #return sym(nabla_grad(u))

def sigma(u):
    return lambda_*nabla_div(u)*fe.Identity(d) + 2*mu*epsilon(u)

# Define variational problem
u = fe.TrialFunction(V)
d = u.geometric_dimension()  # space dimension
v = fe.TestFunction(V)
f = fe.Constant((0, 0, -rho*g))
T = fe.Constant((0, 0, 0))
a = fe.inner(sigma(u), epsilon(v))*fe.dx
L = fe.dot(f, v)*fe.dx + fe.dot(T, v)*fe.ds

# Compute solution
u = fe.Function(V)
fe.solve(a == L, u, bc)



def mesh2triang(mesh):
    xy = mesh.coordinates()
    return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())


# from vtkplotter.dolfin import plot

# plt = plot(u, 
#             mode="displaced mesh",
#             lighting='plastic',
#             axes=1,
#             viewup='z',
#             interactive=0)

# vmesh = plt.actors[0].lineWidth(0)
# vmesh.cutWithPlane(origin=(.5,0,0), normal=(1,2,1))
# plot(vmesh, interactive=1)


uvals = u.vector().get_local()
x = []; y = []; z = [];
for i in range(0, len(uvals), 3):
    x.append(uvals[i])
    y.append(uvals[i+1])
    z.append(uvals[i+2])
print(uvals)
xyzvals = mesh.coordinates()
xvals = xyzvals[:,0]
yvals = xyzvals[:,1]
X, Y = np.meshgrid(x, y)
print(len(X), len(Y))
Z = z
print(len(Z))
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.contour3D(X, Y, Z, 50, cmap='binary')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z');

'''
def plot(obj):
    plt.gca().set_aspect('equal')
    if isinstance(obj, fe.Function):
        mesh = obj.function_space().mesh()
        if (mesh.geometry().dim() != 2):
            raise(AttributeError)
        if obj.vector().size() == mesh.num_cells():
            C = obj.vector().array()
            plt.tripcolor(mesh2triang(mesh), C)
        else:
            C = obj.compute_vertex_values(mesh)
            plt.tripcolor(mesh2triang(mesh), C, shading='gouraud')
    elif isinstance(obj, fe.Mesh):
        if (obj.geometry().dim() != 2):
            raise(AttributeError)
        plt.triplot(mesh2triang(obj), color='k')

# example
fe.mesh = fe.UnitSquareMesh(10, 10)
plt.figure()
fe.plot(mesh)
plt.show()
Q = fe.FunctionSpace(mesh, "CG", 1)
F = fe.interpolate( Q)
fe.plot(F)
plt.show()

'''


'''
# Plot solution
fig, ax = fe.plot(mesh)
ax.set_xlim(0, 1)
plt.show()
fe.plot(u, mode = "glyphs")
plt.show()

# Plot stress
s = sigma(u) - (1./3)*fe.tr(sigma(u))*fe.Identity(d)  # deviatoric stress
von_Mises = fe.sqrt(3./2*fe.inner(s, s))
V = fe.FunctionSpace(mesh, 'P', 1)
von_Mises = fe.project(von_Mises, V)
fe.plot(von_Mises, title='Stress intensity')
plt.show()

# Compute magnitude of displacement
u_magnitude = fe.sqrt(fe.dot(u, u))
u_magnitude = fe.project(u_magnitude, V)
fe.plot(u_magnitude, title='Displacement magnitude')
plt.show()
# array() replaced by get_local()
print('min/max u:',
      u_magnitude.vector().get_local().min(),
      u_magnitude.vector().get_local().max())

# Save solution to file in VTK format
fe.File('elasticity/displacement.pvd') << u
fe.File('elasticity/von_mises.pvd') << von_Mises
fe.File('elasticity/magnitude.pvd') << u_magnitude
'''
