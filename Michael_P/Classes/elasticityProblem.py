import fenics as fe
from ufl import nabla_div

'''
This class is made for salving linear elasitcity problems.
The equation which gets solved is 
    -grad(sigma) = f
    sigma = lambda * tr(epsilon) * id + 2 mu * epsilon
    epsilon = 0.5 * (grad(u) + grad(u)^T)
Where u is the displacement which is searched.

The parameters of the class are
-mesh: contains the space where the equation gets solved
-bc: contains the boundary conditions. 
    Important!!! bc has to be a function with one parameter which is
    a element of VectorFunctionSpace of fenics!!!
    Simple example:
    bc = lambda V : DirichletBC(V, Constant((0, 0, 0)), CompiledSubDomain(...))
-f: force acting on the mesh
-T: traction acting on the surface of the mesh
-dx,ds: Differentials for integrating (by default normal differentials of fenics)
-lamda_, mu, rho: parameters of the selected element

For more information of the governing equations go to:
https://fenicsproject.org/pub/tutorial/html/._ftut1008.html
'''
class ElasticityProblem:
    def __init__(self, mesh, bc=None, f=fe.Constant((0, 0, 0)), T=fe.Constant((0, 0, 0))
                 , dx=[fe.dx], ds=[fe.ds], lambda_=1.25, mu=1, rho=1):
        self.mesh = mesh
        
        # Scaled variables
        self.mu = mu
        self.rho = rho
        self.lambda_ = lambda_
        
        self.dx = dx
        self.ds = ds
        
        self.V = fe.VectorFunctionSpace(self.mesh, 'P', 1)
        if(bc != None):
            self.__initBC(bc)
        else:
            self.bc = None
        self.f = f
        self.T = T
        
        self.u = None
        
    
    def setBC(self, bc):
        self.__initBC(bc)

        
    def setF(self, f):
        self.f = f
        
    def setT(self, T):
        self.T = T
        
    def __initBC(self, bc):
        if(isinstance(bc, list)):
            self.bc = []
            for i in range(len(bc)):
                self.bc.append(bc[i](self.V))
        else:
            self.bc = bc(self.V)
        
    def __defineVariationalProblem(self):
        self.u = fe.TrialFunction(self.V)
        self.d = self.u.geometric_dimension()  # space dimension
        self.v = fe.TestFunction(self.V)
        
        if(not isinstance(self.f, list) and not isinstance(self.T, list)):
        
            self.a = fe.inner(self.__sigma(self.u), self.__epsilon(self.v))*self.dx[0]
            self.L = fe.dot(self.f, self.v)*self.dx[0] + fe.dot(self.T, self.v)*self.ds[0]
        
        
        else:
            self.a = fe.inner(self.__sigma(self.u), self.__epsilon(self.v))*self.dx[0]
            self.L = fe.dot(self.f[0], self.v)*self.dx[0] + fe.dot(self.T[0], self.v)*self.ds[0]
            
            for i in range(1, len(self.dx)):
                self.a += fe.inner(self.__sigma(self.u), self.__epsilon(self.v))*self.dx[i]
                self.L += fe.dot(self.f[i], self.v)*self.dx[i]
                
            for i in range(1, len(self.ds)):
                self.L += fe.dot(self.T[i], self.v)*self.ds[i]
     
    def __epsilon(self, u):
        return 0.5*(fe.nabla_grad(u) + fe.nabla_grad(u).T)

    def __sigma(self, u):
        return self.lambda_*nabla_div(self.u)*fe.Identity(self.d) +\
                2 * self.mu * self.__epsilon(self.u)
    
    def solve(self, fileName, path='.'):
        
        self.__defineVariationalProblem()
        
        # Compute solution
        u = fe.Function(self.V)
        if(self.bc == None):
            fe.solve(self.a == self.L, u)
        else:
            fe.solve(self.a == self.L, u, self.bc)
        self.u = u
        # Save u as pvd file
        fe.File(path + '/' + fileName + '.pvd') << self.u
    
    def saveVon_Mises(self, fileName, path='.'):   
        if(self.u == None):
            print("Please solve the problem before you compute the von mises stress!!!")
        else:
            # Compute von mises stress
            s = self.__sigma(self.u) - (1./3)*fe.tr(self.__sigma(self.u))*fe.Identity(self.d)  # deviatoric stress
            von_Mises = fe.sqrt(3./2*fe.inner(s, s))
            V = fe.FunctionSpace(self.mesh, 'P', 1)
            von_Mises = fe.project(von_Mises, V)
            fe.File(path + '/' + fileName + '.pvd') << von_Mises
    
    def saveMagnitude(self, fileName, path='.'):
        if(self.u == None):
            print("Please solve the problem before you compute the von magnitude!!!")
        else:
            # Compute magnitude of displacement
            u_magnitude = fe.sqrt(fe.dot(self.u, self.u))
            V = fe.FunctionSpace(self.mesh, 'P', 1)
            u_magnitude = fe.project(u_magnitude, V)    
            fe.File(path + '/' + fileName + '.pvd') << u_magnitude
        
