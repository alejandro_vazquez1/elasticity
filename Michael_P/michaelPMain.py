import fenics as fe
import numpy as np
from Classes.elasticityProblem import ElasticityProblem
from Classes.elastodynamicsProblem import ElastodynamicsProblem
    
def elasticityProblemBoundaryConditions():
    # '''
    # Geometry and constants
    # '''
    L = 1000; W = 40
    mesh = fe.Mesh('square.xml')
    delta = W/L
    g = 0.4*delta**2
    rho = 2.5
    
    
    # Subdomians
    left =  fe.CompiledSubDomain("near(x[2], side) && on_boundary", side = 0.0)
    right = fe.CompiledSubDomain("near(x[2], side) && on_boundary", side = 1000.0)
    
    
    # Normal beam on which a torsion is acting
    
    # Define Dirichlet boundary 
    r = fe.Expression(("scale*((x0 + (x[0] - x0)*cos(theta) - (x[1] - y0)*sin(theta)) - x[0])",
                "scale*((y0 + (x[0] - x0)*sin(theta) + (x[1] - y0)*cos(theta)) - x[1])",
                "scale*0.0"),
                scale = 1, x0 = 0, y0 = 0, theta = np.pi/2, degree=2)
    l = fe.Expression(("scale*((x0 + (x[0] - x0)*cos(theta) - (x[1] - y0)*sin(theta)) - x[0])",
                "scale*((y0 + (x[0] - x0)*sin(theta) + (x[1] - y0)*cos(theta)) - x[1])",
                "scale*0.0"),
                scale = 1, x0 = 0, y0 = 0, theta = -np.pi/2, degree=2)
    
    bcl = lambda V : fe.DirichletBC(V, l, left)
    bcr = lambda V : fe.DirichletBC(V, r, right)
    bcs = lambda V : [bcl(V), bcr(V)]
    f = fe.Constant((0, 0, 0))
    T = fe.Constant((0, 0, 0))
    square = ElasticityProblem(mesh, bcs, f, T)
    square.solve('squareWithBCTorsion', 'Output')
    
    
    # pressure
    bcl = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 10)), left)
    bcr = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, -10)), right)
    bcs = lambda V : [bcl(V), bcr(V)]
    square.setBC(bcs)
    square.solve('squareWithBCPressure', 'Output')
    
    # shear force
    bcl = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, -10)), left)
    bcr = lambda V : fe.DirichletBC(V, fe.Constant((0, 0,  10)), right)
    bcs = lambda V : [bcl(V), bcr(V)]
    square.setBC(bcs)
    square.solve('squareWithBCShear', 'Output')
    
    # bending
    bcl = lambda V : fe.DirichletBC(V, fe.Constant((10, 0, 0)), left)
    # def middle(x): return fe.near(x[0], L/2) and (fe.near(x[1], 0.) or fe.near(x[1], W)) and (fe.near(x[2], 0.) or fe.near(x[2], W))
    def middle(x): return fe.near(x[2], 0.) 
    bcm = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), middle)
    bcr = lambda V : fe.DirichletBC(V, fe.Constant((10, 0, 0)), right)
    bcs = lambda V : [bcl(V), bcm(V), bcr(V)]
    square.setBC(bcs)
    square.solve('squareWithBCBending', 'Output')
    
def elasticityProblemForces():
    # '''
    # Geometry and constants
    # '''
    # The force is the gravitational force m*g divided by the volume
    # --> rho*g
    L = 1000; W = 40
    mesh = fe.Mesh('square.xml')
    delta = W/L
    g = 0.4*delta**2
    rho = 2.5
    
    
     # '''
     # Normal beam with different forces on the side 
     # '''

    # Sub domain left end
    def left(x, on_boundary):
        return fe.near(x[2], 0.) and on_boundary
    
    # Sub domain right end
    def right(x, on_boundary):
        return fe.near(x[2], 1000.) and on_boundary
    
    # Sub domain middle end
    def middle(x, on_boundary):
        return on_boundary and fe.between(x[2], (490., 510.))
    
    # Normal beam on which the gravitational force is acting    
    bc = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), left)
    f = fe.Constant((-g*rho*0.001, 0, 0))
    T = fe.Constant((0, 0, 0))
    square = ElasticityProblem(mesh, bc, f, T)
    square.solve('squareWithForceGravitation', 'Output')   
    square.saveVon_Mises('squareWithForceGravitationVonMises', 'Output')   
    square.saveMagnitude('squareWithForceGravitationMagnitude', 'Output')  
    
    
    # # Create mesh function over the cell facets
    # boundary_subdomains = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    # boundary_subdomains.set_all(0)
    # forceBoundaryLeft = fe.AutoSubDomain(left)
    # forceBoundaryLeft.mark(boundary_subdomains, 1)
    # forceBoundaryRight = fe.AutoSubDomain(right)
    # forceBoundaryRight.mark(boundary_subdomains, 2)
    #  # Define measure for boundary condition integral
    # dss = fe.ds(subdomain_data=boundary_subdomains)
    # ds = [dss(2)]
    
    # # boundary conditions
    # bcm = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), middle)
    # bcl = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), left)
    # bcr = lambda V : fe.DirichletBC(V, fe.Constant((0, 0,  0)), right)
    # bcs = lambda V : [bcl(V)]
    
    
     
    #  # pressure
    # f = [fe.Constant((0, 0, 0))]
    # T = [ fe.Constant((0, 0, -0.5))]
    # square = ElasticityProblem(mesh, f=f, bc=bcs, T=T, ds=ds)
    # square.solve('squareWithForcePressure', 'Output')
    
    # # tension
    # T = [ fe.Constant((0, 0, 0.5))]
    # square.setT(T)
    # square.solve('squareWithForceTension', 'Output')
    
    # # shear force
    # T = [ fe.Constant((0, 0.0001, 0))]
    # square.setT(T)
    # square.solve('squareWithForceShear', 'Output')
    
    # # torsion
    # T = [fe.Expression(("scale*((x0 + (x[0] - x0)*cos(theta) - (x[1] - y0)*sin(theta)) - x[0])",
    #             "scale*((y0 + (x[0] - x0)*sin(theta) + (x[1] - y0)*cos(theta)) - x[1])",
    #             "scale*0.0"),
    #             scale = 0.0001, x0 = 0, y0 = 0, theta = -np.pi/2, degree=2)]
    # square.setT(T)
    # square.solve('squareWithForceTorsion', 'Output')
    
    
    # # bending 
    # bc = [ lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), middle) ]
    # f = fe.Constant((-g*rho*0.001, 0, 0))
    # T = fe.Constant((0, 0, 0))
    # square.setT(T)
    # square.setF(f)
    # square.setBC(bc)
    # square.solve('squareWithForceBending', 'Output')

def elastodynamicsPr():
    # Define mesh
    mesh = fe.BoxMesh(fe.Point(0., 0., 0.), fe.Point(1., 0.1, 0.04), 60, 10, 5)
    # Time-stepping parameters
    T       = 4.0
    Nsteps  = 50
    dt = fe.Constant(T/Nsteps)
    
    p0 = 1.
    cutoff_Tc = T/5
    # Define the loading as an expression depending on t
    p = fe.Expression(("0", "t <= tc ? p0*t/tc : 0", "0"), t=0, tc=cutoff_Tc, p0=p0, degree=0)

    # Sub domain for clamp at left end
    def left(x, on_boundary):
        return fe.near(x[0], 0.) and on_boundary
    
    # Sub domain for rotation at right end
    def right(x, on_boundary):
        return fe.near(x[0], 1.) and on_boundary
    
    # Create mesh function over the cell facets
    boundary_subdomains = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    boundary_subdomains.set_all(0)
    force_boundary = fe.AutoSubDomain(right)
    force_boundary.mark(boundary_subdomains, 3)
    
    # Define measure for boundary condition integral
    dss = fe.ds(subdomain_data=boundary_subdomains)
    
    # Set up boundary condition at left end
    zero = fe.Constant((0.0, 0.0, 0.0))
    bc = lambda V : fe.DirichletBC(V, zero, left)
    beamDynamics = ElastodynamicsProblem(mesh, bc, p, ds=dss(3))
    beamDynamics.solve('elastodynamicsTest1', 'Output')
    
    beamDynamics.plotEnergies()
    beamDynamics.plotTip()
    
    
    #change force

     
if __name__ == "__main__":
    #elasticityProblemBoundaryConditions()
    elasticityProblemForces()
    #elastodynamicsPr() 

    
    

   