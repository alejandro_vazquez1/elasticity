import fenics as fe
import numpy as np
from Classes.elasticityProblem import ElasticityProblem
from Classes.elastodynamicsProblem import ElastodynamicsProblem

def clamped_boundary(x, on_boundary):
    tol = 1E-14
    return on_boundary and x[0] < tol
  

class RightSide(fe.SubDomain):
    def inside(self, x, on_boundary):
        return (fe.between(x[0], (0.9, 1.0)) )

def elasticityPrWithSubdomatins():
    # '''
    # Geometry and constants
    # '''
    L = 1; W = 0.2
    mesh = fe.BoxMesh(fe.Point(0, 0, 0), fe.Point(L, W, W), 10, 3, 3)
    delta = W/L
    g = 0.4*delta**2
    rho = 1
    
    rightSide = RightSide()
    
    # Initialize mesh function for interior domains
    # Get all the cells from the mesh
    domains = fe.MeshFunction("size_t", mesh, 0)
    # Indicate all cells with 0
    domains.set_all(0)
    # Indicate our specific region with 1
    rightSide.mark(domains, 1)
    
    # NOTE: for boundaries use dim = 1 instead of dim = 0
    
    fLeft = fe.Constant((0, 0, 0))
    fRight = fe.Constant((0, 0, -rho*g))
    
    # Define new measures associated with the interior domains
    dx = fe.Measure("dx")(subdomain_data=domains)
    # For the exterior boundaries we would need 
    #ds = Measure("ds")[boundaries]
    
    
    bc = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), clamped_boundary)
    #f = fe.Constant((0, 0, -rho*g))
    tol = 1E-14
    fLeft = fe.Constant((0, 0, -rho*g))
    fRight = fe.Constant((0, 0, -rho*g))
    force=-rho*g
    f = fe.Expression(("0.0", "0.0", "x[0] <= 0.95 ? 0 : f"), f=force, degree=2)
    T = fe.Constant((0, 0, 0))
    beam = ElasticityProblem(mesh, bc, f, T)
    beam.solve('testSubdomain', 'Output')
    
  
def elasticityPr():
    # '''
    # Geometry and constants
    # '''
    L = 1; W = 0.2
    mesh = fe.BoxMesh(fe.Point(0, 0, 0), fe.Point(L, W, W), 10, 3, 3)
    V = fe.VectorFunctionSpace(mesh, 'P', 1)
    delta = W/L
    g = 0.4*delta**2
    rho = 1
    
    
    # '''
    # Normal beam on which the gravitational force is acting
    # '''
    
    bc = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), clamped_boundary)
    f = fe.Constant((0, 0, -rho*g))
    T = fe.Constant((0, 0, 0))
    beam = ElasticityProblem(mesh, bc, f, T)
    beam.solve('beamWithGravitationDis', 'Output')
    
    # '''
    # Normal beam on which a torsion is acting
    # '''
    
    # Mark boundary subdomians
    left =  fe.CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.0)
    right = fe.CompiledSubDomain("near(x[0], side) && on_boundary", side = 1.0)
    # Define Dirichlet boundary (x = 0 or x = 1)
    r = fe.Expression(("scale*0.0",
                "scale*((y0 + (x[1] - y0)*cos(theta) - (x[2] - z0)*sin(theta)) - x[1])",
                "scale*((z0 + (x[1] - y0)*sin(theta) + (x[2] - z0)*cos(theta)) - x[2])"),
                scale = 1, y0 = W/2, z0 = W/2, theta = np.pi/3, degree=2)
    l = fe.Expression(("scale*0.0",
                "scale*((y0 + (x[1] - y0)*cos(theta) - (x[2] - z0)*sin(theta)) - x[1])",
                "scale*((z0 + (x[1] - y0)*sin(theta) + (x[2] - z0)*cos(theta)) - x[2])"),
                scale = 1, y0 = W/2, z0 = W/2, theta = -np.pi/3, degree=2)
    
    bcl = lambda V : fe.DirichletBC(V, l, left)
    bcr = lambda V : fe.DirichletBC(V, r, right)
    bcs = lambda V : [bcl(V), bcr(V)]
    f = fe.Constant((0, -rho*g*5, 0))
    beam.setBC(bcs)
    beam.setF(f)
    beam.solve('beamWithTorsionDis', 'Output')
    
    
    # '''
    # Normal beam with different forces on the side 
    # '''
    # Mark boundary subdomians
    left =  fe.CompiledSubDomain("near(x[0], side) && on_boundary", side = 0.0)
    right = fe.CompiledSubDomain("near(x[0], side) && on_boundary", side = 1.0)
    
    # pressure
    bcl = lambda V : fe.DirichletBC(V, fe.Constant((0.1, 0, 0)), left)
    bcr = lambda V : fe.DirichletBC(V, fe.Constant((-0.1, 0, 0)), right)
    bcs = lambda V : [bcl(V), bcr(V)]
    beam.setBC(bcs)
    beam.solve('beamWithPressure', 'Output')
    
    # shear force
    bcl = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, -0.1)), left)
    bcr = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0.1)), right)
    bcs = lambda V : [bcl(V), bcr(V)]
    beam.setBC(bcs)
    beam.solve('beamWithShearForce', 'Output')
    
    # # bending
    # bcl = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0.1)), left)
    # # def middle(x): return fe.near(x[0], L/2) and (fe.near(x[1], 0.) or fe.near(x[1], W)) and (fe.near(x[2], 0.) or fe.near(x[2], W))
    # def middle(x): return fe.near(x[2], 0.) 
    # bcm = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0)), middle)
    # bcr = lambda V : fe.DirichletBC(V, fe.Constant((0, 0, 0.1)), right)
    # bcs = lambda V : [bcl(V), bcm(V), bcr(V)]
    # beam.setBC(bcs)
    # beam.solve('beamWithBending', 'Output')

def elastodynamicsPr():
    # Define mesh
    mesh = fe.BoxMesh(fe.Point(0., 0., 0.), fe.Point(1., 0.1, 0.04), 60, 10, 5)
    # Time-stepping parameters
    T       = 4.0
    Nsteps  = 50
    dt = fe.Constant(T/Nsteps)
    
    p0 = 1.
    cutoff_Tc = T/5
    # Define the load as an expression depending on t
    p = fe.Expression(("0", "t <= tc ? p0*t/tc : 0", "0"), t=0, tc=cutoff_Tc, p0=p0, degree=0)

    # Sub domain for clamp at left end
    def left(x, on_boundary):
        return fe.near(x[0], 0.) and on_boundary
    
    # Sub domain for rotation at right end
    def right(x, on_boundary):
        return fe.near(x[0], 1.) and on_boundary
    
    # Create mesh function over the cell facets
    boundary_subdomains = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    boundary_subdomains.set_all(0)
    force_boundary = fe.AutoSubDomain(right)
    force_boundary.mark(boundary_subdomains, 3)
    
    # Define measure for boundary condition integral
    dss = fe.ds(subdomain_data=boundary_subdomains)
    
    # Set up boundary condition at left end
    zero = fe.Constant((0.0, 0.0, 0.0))
    bc = lambda V : fe.DirichletBC(V, zero, left)
    beamDynamics = ElastodynamicsProblem(mesh, bc, p, ds=dss(3))
    beamDynamics.solve('elastodynamicsTest1', 'Output')
    
    beamDynamics.plotEnergies()
    beamDynamics.plotTip()
    
def elastodynamicsPr2():
    # Define mesh
    mesh = fe.BoxMesh(fe.Point(0., 0., 0.), fe.Point(1., 0.1, 0.04), 60, 10, 5)
    # Time-stepping parameters
    T       = 4.0
    Nsteps  = 50
    dt = fe.Constant(T/Nsteps)
    
    p0 = 0.1
    cutoff_Tc = 4*T/5
    # Define the load as an expression depending on t
    #p = fe.Expression(("0", "t <= tc ? p0*t/tc : 0", "0"), t=0, tc=cutoff_Tc, p0=p0, degree=0)
    p = fe.Expression(("0", "0", 
                       "(t <= tc) and (x[0]-tol) <=  t/tc and (x[0] + tol >= t/tc) ? p0: 0",),
                      t=0, tol=0.03, tc=cutoff_Tc, p0=p0, degree=0)
    #f = fe.Expression(("0.0", "0.0", "x[0] <= 0.95 ? 0 : f"), f=force, degree=2)

    # Sub domain for clamp at left end
    def left(x, on_boundary):
        return fe.near(x[0], 0.) and on_boundary
    
    # Sub domain for rotation at right end
    def right(x, on_boundary):
        return fe.near(x[0], 1.) and on_boundary
    
    # Create mesh function over the cell facets
    boundary_subdomains = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
    boundary_subdomains.set_all(0)
    force_boundary = fe.AutoSubDomain(right)
    force_boundary.mark(boundary_subdomains, 3)
    
    # Define measure for boundary condition integral
    dss = fe.ds(subdomain_data=boundary_subdomains)
    
    # Set up boundary condition at left end
    zero = fe.Constant((0.0, 0.0, 0.0))
    bcl = lambda V : fe.DirichletBC(V, zero, left)
    bcr = lambda V : fe.DirichletBC(V, zero, right)
    bcs = lambda V : [bcl(V), bcr(V)]
    beamDynamics = ElastodynamicsProblem(mesh, bcs, p)
    beamDynamics.solve('elastodynamicsTest2', 'Output')
    
    beamDynamics.plotEnergies()
    beamDynamics.plotTip()

     
if __name__ == "__main__":
    #elasticityPr()
    #elastodynamicsPr() 
    elastodynamicsPr2() 
    #elasticityPrWithSubdomatins()
    

    
    

   