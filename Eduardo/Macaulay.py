# -*- coding: utf-8 -*-
"""
Created on Sun Jan  3 23:02:32 2021

@author: evu09
"""

import numpy as np
import matplotlib.pyplot as plt

'''constants'''
#beam simensions
basis = 20
hight = 20
Length = 100
#
E_Modul = 70 #70 kN/mm**2 Aluminuim E-module
Presure = 5
num = 10 # number of steps

def dy(I, E, L, P, n):

    ''''Symetrical appliied load in the middle M(x)=Px/2 and M(x)=Px/2 - P(L-x)/2'''
    ''' Macaulay's method for Euler-Bernoulli beam'''
    
    c1 = -(P*L**2)/16
    #print (c1)
    c2 = 0
    c3 = c1 - (P*L**2)/8
    #print (c3)
    c4 = (P*L**3)/48
    #print (c4)
    '''solutions of the integrals'''
    #for 0 <= x <= L
    x1 = np.linspace(0, L/2, n)
    y1 = (1/E*I)*(((P/12)*x1**3) + (c1*x1) + c2)
    
    #for L/2 <= x <= L
    x2 = np.linspace(L/2, L, n)
    y2 = (1/E*I)*(((-P/12)*x2**3) + ((P*L/4)*x2**2) + (c3*x2) + c4)
    
    y_max = (-P*L**3)/(48*E*I)
    
    #print (y_max, y1[-1]/y_max, y2[0])
    dif = y1[-1]/y_max
    x = np.concatenate((x1, x2), axis=None)
    y = np.concatenate((y1, y2), axis=None)
    
    return y/dif, x




def dw(I, E, L, P, n):

    ''''Symetrical appliied load in the middle M(x)=Px/2 and M(x)=Px/2 - P(L-x)/2'''
    ''' Macaulay's method for Euler-Bernoulli beam'''
    '''solutions of the integrals'''
    #for 0 <= x <= L
    x1 = np.linspace(0, L/2, n)
    #wikipedia
    w1 = -((P*x1)*((4*x1**2)-(3*L**2)))/(48*E*I)
    
    
    #for L/2 <= x <= L
    x2 = np.linspace(L/2, L, n)
    #wikipedia
    w2 = ((P)*(x2-L)*((4*x2**2)-(8*L*x2)+(L**2)))/(48*E*I)
    
    #y_max = (-P*L**3)/(48*E*I)
    
    x = np.concatenate((x1, x2), axis=None)
    w = np.concatenate((w1, w2), axis=None)

    return w, x

def I_rbeam(b,h): #Rectangular beam. in y direction Iy because presure is in y direction
    I=(b**3.)*h/12.
    return I

for i in range(Presure):
    y_f, xy = dy(I_rbeam(basis,hight), E_Modul, Length, i, num)
    w_f, xw = dw(I_rbeam(basis,hight), E_Modul, Length, i, num)
    plt.plot(xy, y_f, label='P=-%i' %(i))
    plt.plot(xw, w_f, label='P=%i' %(i))
#plt.set_title('Aluminium beam')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
plt.xlabel('x')
plt.ylabel('y')
plt.grid(True)
#plt.axis('square')
plt.savefig('Al_beam.png')
plt.show()






