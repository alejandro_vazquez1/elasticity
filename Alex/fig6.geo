// Point measure
mesh = 5.0;


//create 5 Points
Point(1) = {0, 0, 0 , mesh};
Point(2) = {10, 0, 0, mesh};
Point(3) = {0, 10, 0, mesh};
Point(4) = {-10, 0, 0, mesh};
Point(5) = {0, -10, 0, mesh};

//Circle arcs

Circle(1) = {2, 1, 3};
Circle(2) = {3, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 2};

Line Loop(1) = {2, 3, 4, 1};
Plane Surface(1) = {1};

object[] = Extrude{0, 0, 1000}{Surface{1};};
Delete{ Volume{object[1]};}
Surface Loop(1) = {1, object[0],object[2] ,object[3] ,object[4], object[5]};

Volume(100) = {1};

Physical Volume(1) = {1};
Physical Surface(1) = {1};


